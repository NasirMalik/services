package com.nasir.services.spring.service;

import com.nasir.persistence.model.Book;
import com.nasir.persistence.model.CD;
import com.nasir.services.spring.ServicesConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

class PersistServiceTest {

    private static PersistService service;

    static int book_Id;
    static int cd_Id;

    @BeforeAll
    static void setUp() {
        service = new AnnotationConfigApplicationContext(ServicesConfig.class).getBean(PersistService.class);
    }

    @Test
    void shouldCreateBook() {
        book_Id = service.createBook(new Book("Test Book 1", "Long Description for the Book", 35F, "345345345345", (short) 450));
        System.out.println(book_Id);
    }

    @Test
    void shouldCreateCD() {
        CD cd = new CD("Test CD 1", "Long Description for the CD", "Test Genre", 450F);
        cd.setCost(23F);
        cd_Id = service.createCD(cd);
        System.out.println(cd_Id);
    }
}