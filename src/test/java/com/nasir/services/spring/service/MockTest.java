package com.nasir.services.spring.service;

import com.nasir.persistence.model.Book;
import com.nasir.persistence.model.CD;
import com.nasir.services.spring.repository.PersistenceDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

public class MockTest {

    @InjectMocks
    private PersistServiceImpl service;

    @Mock
    private PersistenceDao dao;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldMockCDCreation() {
        CD cd = new CD();
        cd.setId(2);
        when(dao.create(cd)).thenReturn(cd.getId());
        Assertions.assertEquals(2, service.createCD(cd));
    }

    @Test
    public void shouldMockBookCreation() {
        Book book = new Book();
        book.setId(12);
        when(dao.create(book)).thenReturn(book.getId());
        Assertions.assertEquals(12, service.createBook(book));
    }
}
