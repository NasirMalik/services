package com.nasir.services.jdbc;

import com.nasir.persistence.model.Book;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

class JDBCServiceTest {

    JDBCService JDBCService;

    @BeforeEach
    void setUp() {
        JDBCService = new JDBCService();
    }

    @AfterEach
    void tearDown() {
        JDBCService = null;
    }

    @Test
    void shouldPersistBook() throws SQLException {
        Book book = new Book("Test Book 1", "Long Description for the Book", 35F, "345345345345", (short) 450);
        book.setId(1);
        JDBCService.persistBook(book);
    }

    @Test
    void shouldFindBook() {
        Book book = JDBCService.findBook(1);
        System.out.println(book);
    }
}