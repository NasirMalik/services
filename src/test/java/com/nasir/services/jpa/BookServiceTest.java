package com.nasir.services.jpa;

import com.nasir.persistence.model.Book;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BookServiceTest {

    BookService service;
    static int book_Id;

    @BeforeEach
    void setUp() {
        service = new BookService();
    }

    @AfterEach
    void tearDown() {
        service.shutDown();
        service = null;
    }

    @Test
    @Order(1)
    void shouldCreate() {
        book_Id = service.create(new Book("Test Book 1", "Long Description for the Book", 35F, "345345345345", (short) 450));
    }

    @Test
    @Order(2)
    void shouldRead() {
        Book book = service.read(book_Id);
        System.out.println(book);
    }

    @Test
    @Order(3)
    void shouldUpdate() {
        service.update(book_Id, "New Name");
    }

    @Test
    @Order(4)
    void shouldDelete() {
        service.delete(book_Id);
    }

}