package com.nasir.services.jpa;

import com.nasir.persistence.model.CD;
import org.junit.jupiter.api.*;

import javax.persistence.RollbackException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CDServiceTest {

    CDService service;
    static int cd_Id;

    @BeforeEach
    void setUp() {
        service = new CDService();
    }

    @AfterEach
    void tearDown() {
        service.shutDown();
        service = null;
    }

    @Test
    @Order(1)
    void shouldCreate() {
        CD cd = new CD("Test CD 1", "Long Description for the CD", "Test Genre", 450F);
        cd.setCost(23F);

//        Edition edition = new Edition(1, "First CD Edition for CD", LocalDate.of(2011, 10, 3));
//        edition.getId().setCd(cd);
//        cd.getEditions().add(edition);r

//        cd.setLeadMusician(new Musician("Test First Name", "Test 11", "A Lot", LocalDate.now(), 25F));
//
//        cd.getMusicians().add(new Musician("Test First Name", "Test 2", "A Lot", LocalDate.now(), 25F));
//        cd.getMusicians().add(new Musician("Test First Name", "Test 3", "A Lot", LocalDate.now(), 25F));

        try {
            cd_Id = service.create(cd);
        } catch (RollbackException rbe) {
            rbe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(2)
    void shouldRead() {
        CD cd = service.read(1);
        System.out.println(cd);
    }

    @Test
    @Order(3)
    void shouldUpdate() {
        CD cd = service.read(cd_Id);
        service.update(cd, "New Title");
    }

    @Test
    @Order(4)
    void shouldDelete() {
        CD cd = service.read(cd_Id);
        service.delete(cd);
    }
}