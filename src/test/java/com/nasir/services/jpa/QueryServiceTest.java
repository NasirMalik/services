package com.nasir.services.jpa;

import com.nasir.persistence.model.CD;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QueryServiceTest {

    private QueryService service;

    @BeforeEach
    void setUp() {
        service = new QueryService();
    }

    @AfterEach
    void tearDown() {
        service.shutDown();
        service = null;
    }

    @Test
    void shouldQueryItems() {
        assertAll(
                () -> assertEquals(0, service.getAllCDs().size()),
                () -> assertNotEquals(0, service.getAllBooks().size()),
                () -> assertEquals("Test Book 1", service.getBook1Name())
        );
    }

    @Test
    void shouldTestNamedQueryforCD() {
        CD cd = (CD) service.getCDNamed(1);
        assertNull(cd);
//        System.out.println(cd);
//        System.out.println(cd.getMusicians().size());
//        System.out.println(cd.getEditions().iterator().next());
    }

    @Test
    void shouldTestNamedQueryforAllCD() {
        service.getAllCDsNamed().forEach(System.out::println);
    }
}