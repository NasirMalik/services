package com.nasir.services.jdbc;

import com.nasir.persistence.model.Book;

import java.sql.*;

public class JDBCService {

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/jpatest?serverTimezone=Australia/Sydney","root","root");
    }

    public Book findBook(int id) {
        Book book = new Book();
        String query = "Select id, name, description, cost, isbn, noOfPages from book where id = ?";
        try (PreparedStatement stmt = getConnection().prepareStatement(query)){
            stmt.setInt(1, id);
            ResultSet rst = stmt.executeQuery();

            while (rst.next()) {
                book.setId(rst.getInt(1));
                book.setName(rst.getString(2));
                book.setDescription(rst.getString(3));
                book.setCost(rst.getFloat(4));
                book.setIsbn(rst.getString(5));
                book.setNoOfPages(rst.getShort(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return book;
    }

    public void persistBook(Book book) throws SQLException {
        String query = "Insert into book (id, name, description, cost, isbn, noOfPages) VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = getConnection().prepareStatement(query)){
            stmt.setInt(1, book.getId());
            stmt.setString(2, book.getName());
            stmt.setString(3, book.getDescription());
            stmt.setFloat(4, book.getCost());
            stmt.setString(5, book.getIsbn());
            stmt.setShort(6, book.getNoOfPages());

            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

}
