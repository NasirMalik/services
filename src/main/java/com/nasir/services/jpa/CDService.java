package com.nasir.services.jpa;

import com.nasir.persistence.model.CD;

public class CDService extends BaseService {

    public CDService() {
        super();
    }

    public int create(CD cd) {
        tx.begin();
        em.persist(cd);
        tx.commit();
        return cd.getId();
    }

    public CD read(int id) {
        return em.find(CD.class, id);
    }

    public void update(int id, String newTitle) {
        CD cd = read(id);
        if (cd != null) {
            tx.begin();
            cd.setTitle(newTitle);
            tx.commit();
        }
    }

    public void update(CD cd, String newTitle) {
        CD cd1 = em.merge(cd);
        if (cd != null) {
            tx.begin();
            cd1.setTitle(newTitle);
            tx.commit();
        }
    }

    public void delete(int id) {
        CD cd = read(id);
        if (cd != null) {
            tx.begin();
            em.remove(cd);
            tx.commit();
        }
    }

    public void delete(CD cd) {
        CD cd1 = em.merge(cd);
        if (cd != null) {
            tx.begin();
            em.remove(cd1);
            tx.commit();
        }
    }

}
