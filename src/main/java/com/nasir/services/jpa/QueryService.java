package com.nasir.services.jpa;

import com.nasir.persistence.model.Book;
import com.nasir.persistence.model.CD;
import com.nasir.persistence.model.Item;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class QueryService extends BaseService {

    public QueryService() {
        super();
    }

    public List<? extends Item> getAllBooks() {
        return getAll("SELECT b From Book b", Book.class);
    }

    public List<? extends Item> getAllCDs() {
        return getAll("SELECT c From CD c", CD.class);
    }

    public List<? extends Item> getAllCDsNamed() {
        return getAllNamed("ALL", CD.class);
    }

    public Object getCD(int i) {
        return getOne("SELECT c FROM CD c WHERE c.id = ?1", i);
    }

    public Object getCDNamed(int i) {
        Object obj = getOneNamed("CD", i);
        return obj;
    }

    public String getBook1Name() {
        Object obj = getOne("SELECT b.name FROM Book b WHERE b.id = 1");
        return obj == null ? "" : obj.toString();
    }

    // Base Queries
    private List<? extends Item> getAll(String query, Class className, Object... params) {
        TypedQuery typedQuery = em.createQuery(query, className);
        for (int i = 0; i < params.length; i++) {
            typedQuery.setParameter(i + 1, params[i]);
        }
        return typedQuery.getResultList();
    }

    private List<?> getAll(String query, Object... params) {
        Query query1 = prepareQuery(query, params);
        return query1.getResultList();
    }

    private Object getOne(String query, Object... params) {
        Query query1 = prepareQuery(query, params);
        try {
            return query1.getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Query prepareQuery(String query, Object[] params) {
        Query query1 = em.createQuery(query);
        for (int i = 0; i < params.length; i++) {
            query1.setParameter(i + 1, params[i]);
        }
        return query1;
    }

    private List<? extends Item> getAllNamed(String query, Class className, Object... params) {
        TypedQuery typedQuery = em.createNamedQuery(query, className);
        for (int i = 0; i < params.length; i++) {
            typedQuery.setParameter(i + 1, params[i]);
        }
        return typedQuery.getResultList();
    }

    private List<?> getAllNamed(String queryName, Object... params) {
        Query query1 = prepareQueryNamed(queryName, params);
        return query1.getResultList();
    }

    private Object getOneNamed(String queryName, Object... params) {
        Query query1 = prepareQueryNamed(queryName, params);
        try {
            return query1.getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Query prepareQueryNamed(String queryName, Object[] params) {
        Query query1 = em.createNamedQuery(queryName);
        for (int i = 0; i < params.length; i++) {
            query1.setParameter(i + 1, params[i]);
        }
        return query1;
    }



}
