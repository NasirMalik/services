package com.nasir.services.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public abstract class BaseService {

    protected EntityManagerFactory emf;
    protected EntityManager em;
    protected EntityTransaction tx;

    public BaseService() {
        if (em == null) {
            emf = Persistence.createEntityManagerFactory("testjpa-unit");
            em = emf.createEntityManager();
            tx = em.getTransaction();
        }
    }

    public void shutDown() {
        em.close();
        emf.close();
    }
}
