package com.nasir.services.jpa;

import com.nasir.persistence.model.Book;

public class BookService extends BaseService {

    public BookService() {
        super();
    }

    public int create(Book book) {
        tx.begin();
        em.persist(book);
        tx.commit();
        return book.getId();
    }

    public Book read(int id) {
        return em.find(Book.class, id);
    }

    public void update(int id, String newName) {
        Book book = read(id);
        if (book != null) {
            tx.begin();
            book.setName(newName);
            tx.commit();
        }
    }

    public void delete(int id) {
        Book book = read(id);
        if (book != null) {
            tx.begin();
            em.remove(book);
            tx.commit();
        }
    }

}
