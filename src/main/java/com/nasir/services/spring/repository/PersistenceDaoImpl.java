package com.nasir.services.spring.repository;

import com.nasir.persistence.model.Book;
import com.nasir.persistence.model.CD;
import org.springframework.stereotype.Repository;

@Repository
public class PersistenceDaoImpl extends PersistenceDao {

    @Override
    public int create(Book book) {
        em.persist(book);
        return book.getId();
    }

    @Override
    public int create(CD cd) {
        em.persist(cd);
        return cd.getId();
    }
}
