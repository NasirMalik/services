package com.nasir.services.spring.repository;

import com.nasir.persistence.model.Book;
import com.nasir.persistence.model.CD;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class PersistenceDao {

    @PersistenceContext
    protected EntityManager em;

    public abstract int create(Book book);

    public abstract int create(CD cd);
}
