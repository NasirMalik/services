package com.nasir.services.spring.service;

import com.nasir.persistence.model.Book;
import com.nasir.persistence.model.CD;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PersistServiceImpl extends BaseSpringService implements PersistService {

    @Override
    public int createBook(Book b) {
        dao.create(b);
        System.out.println("Book Created with Id : " + b.getId());
        return b.getId();
    }

    @Override
    public int createCD(CD cd) {
        dao.create(cd);
        System.out.println("CD Created with Id : " + cd.getId());
        return cd.getId();
    }
}
