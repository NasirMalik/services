package com.nasir.services.spring.service;

import com.nasir.persistence.model.Book;
import com.nasir.persistence.model.CD;

public interface PersistService {

    int createBook(Book b);

    int createCD(CD cd);
}
