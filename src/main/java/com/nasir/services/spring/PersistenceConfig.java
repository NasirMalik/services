package com.nasir.services.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class PersistenceConfig {

    @Bean
    public LocalContainerEntityManagerFactoryBean getEntityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
//        entityManagerFactory.setPersistenceUnitName("testjpa-unit");
        entityManagerFactory.setPackagesToScan(new String[] { "com.nasir.persistence.model", "com.nasir.persistence.model.id" });
        entityManagerFactory.setDataSource(getDataSource());
        entityManagerFactory.setMappingResources("META-INF/listeners.xml");
        entityManagerFactory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        Properties providerProps = new Properties();
        providerProps.put("hibernate.format_sql", "true");
        providerProps.put("hibernate.show_sql", "true");
        providerProps.put("hibernate.hbm2ddl.auto", "update");

        entityManagerFactory.setJpaProperties(providerProps);

        return entityManagerFactory;
    }

    @Bean
    public JpaTransactionManager getJpaTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(getEntityManagerFactoryBean().getObject());
        return transactionManager;
    }

    @Bean
    public DriverManagerDataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/jpatest?serverTimezone=Australia/Sydney");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }

}
